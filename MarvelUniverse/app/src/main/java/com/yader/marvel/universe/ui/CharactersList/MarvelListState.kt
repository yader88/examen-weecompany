package com.yader.marvel.universe.ui.CharactersList

import com.yader.marvel.universe.domain.model.CharacterModel

data class MarvelListState(
    val isLoading : Boolean = false,
    val charactersList : List<CharacterModel> = emptyList(),
    val error : String = ""
)