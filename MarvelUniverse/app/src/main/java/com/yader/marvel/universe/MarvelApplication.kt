package com.yader.marvel.universe

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class MarvelApplication:Application()