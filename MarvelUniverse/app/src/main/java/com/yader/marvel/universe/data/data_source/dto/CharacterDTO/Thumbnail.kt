package com.yader.marvel.universe.data.data_source.dto.CharacterDTO

data class Thumbnail(
    val extension: String,
    val path: String
)